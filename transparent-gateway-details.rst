.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

Transparent Gateway Blueprint Details
#####################################

The following section details about various specifics of the Transparent Gateway Blueprint. The details include the basic networking subnet, configuration, Wi-Fi, and OpenThread connectivity details.

.. _MDK-rcp-flash:

Build and Flash the Radio Co-Processor Firmware
***********************************************

To allow the Raspberry Pi 4 to participate in an OpenThread wireless network, add a radio interface, as the Raspberry Pi 4 lacks on-board radio hardware to support it.

In this blueprint, the Makerdiary nrf52840 MDK USB dongle is used to provide the radio functionality. To enable our use case of an OpenThread border router, you need to compile and flash a fitting firmware image to the device.

The current manual process is quite intricated, but expect vast improvements in future iterations of the blueprint.

- Setup the OpenThread environment as follows:

.. code-block:: bash

   $ docker pull openthread/environment:latest
   $ docker run -it --rm openthread/environment bash

- Inside the docker container prepare the build as follows:

.. code-block:: bash

   $ rm -rf openthread/
   $ git clone --recursive https://github.com/openthread/openthread.git
   $ cd openthread
   $ git checkout thread-reference-20191113-1633-g4d50cbadb -b mdk-rcp
   $ ./bootstrap

- Build the radio co-processor (RCP) firmware image as follows:

.. code-block:: bash

   $ make -f examples/Makefile-nrf52840 USB=1 BOOTLOADER=USB JOINER=1

- Convert the firmware into the correct format for flashing as follows:

.. code-block:: bash

   $ arm-none-eabi-objcopy -O ihex output/nrf52840/bin/ot-rcp output/nrf52840/bin/ot-rcp.hex
   $ wget https://raw.githubusercontent.com/makerdiary/nrf52840-mdk-usb-dongle/master/tools/uf2conv.py
   $ python3 ./uf2conv.py output/nrf52840/bin/ot-rcp.hex -c -f 0xADA52840

The process creates a new file `flash.uf2`, which is the final firmware file needed.

The following two commands need to be executed in a new shell while the docker instance is still running.

- Get container ID for the running instance as follows:

.. code-block:: bash

   $ docker container list

- Insert the container ID from above into the command below to copy the firmware file out of the container into your current directory:

.. code-block:: bash

   $ docker cp $CONTAINER_ID:/openthread/flash.uf2 .

- Flash firmware onto the device as follows:

#. Switch the device into flash mode by holding down the button, while inserting the device into the USB port (a steady green LED next to the button indicates the device is in flash mode).
#. A new USB storage device appears (with name MDK-DONGLE).
#. Copy the `flash.uf2` file into the root folder of the USB storage.
#. Once copied, the bootloader writes the new firmware, and resets the device.
#. After the reset, only the main LED next to the USB plug remains steady green, which signifies that, it is now ready to be placed into the Raspberry Pi 4.

.. _QR-gen:

Generate Onboarding QR Code
***************************

During the OpenThread onboarding process of the mesh node onto the OpenThread network, a QR code is used. The QR code holds the joiner credential, and the unique EUI64 address of the device to join.

The joiner credential can be chosen freely, but ensure that it is identical in the firmware of the node to join, as well as in the QR code.

In this blueprint the joiner credential is: J01NU5

The EUI64, basically the MAC address of the device, is hardware dependent and needs to be fetched from the device after flashing the generated firmware image. After accessing the serial console of the Arduino Nano 33 BLE, issue the *ot eui64* command to receive the address as follows:

.. code-block:: bash

   $ screen /dev/ttyACM0 115200
   uart:~$ ot eui64

The full string needed to be encoded in the QR code looks as follows:

`v=1&&eui=00124b001ca77adf&&cc=J01NU5`

The value for the EUI parameter needs to be replaced with the one received from
the device. The QR code needs to be generated in text format (not as an URL). A simple online QR generator can be found here: https://qrd.by/#text

Network Subnets and Configuration
*********************************

On the basis of the board you use, and its hardware configuration, the available network interfaces and their names may vary. In order to avoid confusion, the following interfaces are assumed to be available:

- **Ethernet interface eth0:** assumed to be uplink with DHCP enabled.
- **Wi-Fi interface wlan0:** Wi-Fi access point interface serving the Wi-Fi subnet.
- **OpenThread interface wpan0:** OpenThread border router interface serving the mesh network.

In terms of IP subnets, use the private 172.16.47.0/24 range on the Wi-Fi subnet. The Access point(AP) has 172.16.47.1/24 assigned to it. Clients are served DHCP leases in the range of 172.16.47.100 - 172.16.47.150. The default DNS servers are 9.9.9.9 (primary) and 8.8.8.8 (secondary) respectively. For IPv6, you can rely on address auto-configuration for the time being.

On the OpenThread mesh network subnet, no IPv4 is available, and again you can rely on address auto-configuration for the time being.

Forwarding for IPv4 and IPv6 is enabled on all interfaces with `sysctl`.

Wi-Fi Access Point Configuration
********************************

Using the default Wi-Fi access point configuration, create an AP on channel 6 in the 2.4 GHz band with WPA2 pre-shared key configuration as:

.. code-block:: bash

   `SSID: "Oniro Project WiFi"`
   `Passphrase: "12345678"`

For more details, refer to the `NetworkManager configuration file <https://booting.oniroproject.org/distro/oniro/-/blob/dunfell/meta-oniro-blueprints/recipes-connectivity/networkmanager/networkmanager-softap-config/SoftAP.nmconnection>`_ to get more clarity.

OpenThread Border Router Configuration
**************************************

In our default OpenThread border router configuration, we create an OpenThread mesh network on channel 26 in the 2.4 GHz band with panid 0x1357:

.. code-block:: bash

   `Networkname "OniroThread"`
   `OpenThread masterkey: 00112233445566778899aabbccddeeff`

For more details, refer to the `OpenThread configuration script <https://booting.oniroproject.org/distro/oniro/-/blob/dunfell/meta-oniro-blueprints/recipes-connectivity/openthread/ot-br-posix/otbr-configuration>`_ for getting more clarity.
