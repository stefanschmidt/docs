.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

Peripherals used in implementing blueprints
###########################################

This section lists some of the peripherals proposed to be used (and supported) as part of |main_project_name| Blueprints.

Peripheral Categories
*********************

.. toctree::
   :maxdepth: 2

   io-devices/index
   lock-devices/index

