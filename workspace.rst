.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ./definitions.rst

.. _Workspace:

Workspace
#########

.. contents::
   :depth: 2

Oniro Project Workspace
***********************

In order to be able to build a blueprints, a set up workspace is required. The
blueprints require a `oniro` workspace so the first step is to follow the
instructions available on `Oniro Project documentation
<https://docs.oniroproject.org/en/latest/oniro/repo-workspace.html>`_

If you already have a workspace in place, make sure you update it by running
`repo sync`.

Blueprints Workspace
********************

A blueprint build is very similar to an `oniro` build but requires additional
layers set up in the build. The general steps are:

#. Define a place for the blueprints layer and its dependencies

   .. code-block:: bash

      $ export CHECKOUT_DIR=~/oniroproject
      $ mkdir -p $CHECKOUT_DIR

#. Clone `meta-oniro-blueprints
   <https://booting.oniroproject.org/distro/meta-oniro-blueprints>`_:

   .. code-block:: bash

      $ cd $CHECKOUT_DIR
      $ git clone https://booting.oniroproject.org/distro/meta-oniro-blueprints
      $ cd meta-oniro-blueprints
      $ git checkout origin/dunfell


#. Clone the dependencies of the `meta-oniro-blueprints` layer that are not
   brought in by `oniro` by default:

   * `meta-homeassistant <https://github.com/meta-homeassistant/meta-homeassistant>`_

     .. code-block:: bash

        $ cd $CHECKOUT_DIR
        $ git clone https://github.com/meta-homeassistant/meta-homeassistant.git
        $ cd meta-homeassistant
        $ git checkout origin/dunfell

   .. note::

      If you already have any of these layers cloned and you want to reuse
      them, just update the revision of the branch above.


#. The first time a build is initialized, after running the `oe-init-build-env`
   script, add that layers to the build's `bblayers.conf` either manually or by
   running:

   .. code-block:: bash

      $ bitbake-layers add-layer $CHECKOUT_DIR/meta-homeassistant
      $ bitbake-layers add-layer $CHECKOUT_DIR/meta-oniro-blueprints

   .. attention::

      Make sure you define (as per the instructions above) or replace
      `CHECKOUT_DIR` accordingly.

      Als, the order above is important as `meta-homeassistant` is a dependency
      of `meta-oniro-blueprints`.

#. Once that is done. Follow the specific blueprint's build instructions.
