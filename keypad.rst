.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ./definitions.rst

KeyPad Blueprint
#####################

.. contents::
   :depth: 4
 
.. attention::
   The `TouchPannel` blueprint is still a work in progress (WIP). For more
   information, see the :ref:`resources <Resources>` section.
    


Overview
********

The KeyPad Blueprint provides support for building a PoC smart touch panel that can be used in other smart devices to accept input and display status. Some features to demonstrate include:

#. Accepting input from a keypad
#. Accepting input from UI icons on a touch panel
#. Ability to send input data securely over the network


.. image:: assets/keypad-nRF52840.jpg
   :alt: Keypad on nRF52840-DK
   
Hardware Setup - Bill of Materials
**********************************

The current version of keypad can be deployed to Nordic's nRF52840-DK

- 1 x `nRF52840-DK
  <https://www.nordicsemi.com/Products/Development-hardware/nRF52840-DK>`_
- 1 x `2.8" TFT Touch Shield for Arduino w/Capacitive Touch
  <https://www.adafruit.com/product/1947>`_
- 1 x Micro USB cable  

   
Get sources
***********

Get or update the |main_project_name| sources as described in the
:ref:`blueprints workspace documentation <Workspace>`.

The quick steps are detailed as follows:

.. code-block:: bash

   $ export CHECKOUT_DIR=~/oniroproject
   $ mkdir -p "${CHECKOUT_DIR}" ; cd "$CHECKOUT_DIR"
   $ # Download sources
   $ repo init -u https://booting.oniroproject.org/distro/oniro -b dunfell
   $ git clone https://booting.oniroproject.org/distro/meta-oniro-blueprints --branch dunfell --depth 1
   $ git clone https://github.com/meta-homeassistant/meta-homeassistant --branch dunfell --depth 1
   $ repo sync
   $ # Configure environement and build directory for supported hardware
   $ TEMPLATECONF=../oniro/flavours/zephyr . ./oe-core/oe-init-build-env build-oniro-zephyr
   $ bitbake-layers add-layer $CHECKOUT_DIR/meta-homeassistant
   $ bitbake-layers add-layer $CHECKOUT_DIR/meta-oniro-blueprints
   $ export MACHINE=nrf52840dk-nrf52840
   $ # Build firmware image
   $ bitbake zephyr-blueprint-keypad
   $ du -hs ./tmp-newlib/deploy/images/nrf52840dk-nrf52840/zephyr-blueprint-keypad.bin # 148K

Then plug the board to USB connector (the one near the battery), then it should be detected by Linux system:

.. code-block:: bash

   $ sudo dmesg
   [...] usb 2-1.7: Product: J-Link
   [...] usb 2-1.7: Manufacturer: SEGGER
   (...)
   $ ls /dev/disk/by-id/usb-SEGGER_MSD_Volume_*
   $ sudo mkdir /tmp/usb
   $ sudo mount  /dev/disk/by-id/usb-SEGGER_MSD_Volume_*-0\:0  /mnt/usb
   $ sudo cp -v /tmp-newlib/deploy/images/nrf52840dk-nrf52840/zephyr-blueprint-keypad.bin /mnt/usb

Display should show the keypad on reset.
   
   
Resources
=========

.. _Resources:

- See `KeyPad's requirement <https://git.ostc-eu.org/OSTC/planning/blueprints/-/issues/1>`_
