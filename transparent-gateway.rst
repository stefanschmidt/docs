.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ./definitions.rst

Transparent Gateway Blueprint
#############################

.. contents::
   :depth: 4

Overview
********

The Transparent Gateway Blueprint provides support for building a Proof of Concept(PoC) IoT
gateway that can be ideally used as the communication hub in a smart home. In addition to the gateway, it also provides a PoC of a mesh node based on Zephyr.

A few of the features demonstrated are as follows:

#. WiFi AP functionality
#. OpenThread Border Router functionality
#. Onboarding of a mesh node in an OpenThread network
#. A mesh node to participate in an OpenThread network

This is the first iteration of the blueprint. It will be updated and extended in
the future.

Hardware Setup - Bill of Materials
**********************************

Gateway:

- 1x `Raspberry Pi 4B board <https://www.raspberrypi.com/products/raspberry-pi-4-model-b/>`_ (>=2GB RAM)
- 1x `Raspberry Pi USB-C Power supply <https://www.raspberrypi.com/products/type-c-power-supply/>`_
- 1x microSD card (>=8GB size)
- 1x `Makerdiary nrf52840 MDK USB dongle <https://wiki.makerdiary.com/nrf52840-mdk-usb-dongle/>`_
- 1x `USB serial cable <https://www.adafruit.com/product/954>`_ (optional)
- 1x Ethernet cable

Mesh node:

- 1x `Arduino Nano 33 BLE <https://store.arduino.cc/arduino-nano-33-ble-sense-with-headers>`_
- 1x Micro USB cable

The setup of the hardware components is illustrated in the hardware setup image:

.. image:: assets/gateway-hw-setup.png
   :alt: Transparent Gateway - Hardware setup

On the Arduino Nano 33 BLE the only connection needed is the micro USB cable which is used for power, flashing, and serial console.

For the Raspberry Pi 4 you need to perform a few more bits for completing the setup.

- Connect the ethernet cable and insert the Makerdiary MDK USB dongle in one of the USB ports.
- Insert the micro SD card, and the USB-C power supply can wait until the software is build and flashed.

Optionally, a USB to TTL serial cable can be connected as a serial console for debugging. A detailed description on connecting the cable can be found `on the conect the lead page <https://learn.adafruit.com/adafruits-raspberry-pi-lesson-5-using-a-console-cable/connect-the-lead>`_. An alternative to the serial console is to SSH over the local network connection.

Fetch Blueprint Sources and Build
*********************************

Fetch the Build Metadata
------------------------

The final piece remaining after building the hardware setup is fetching the
software sources and building the OS.

For fetching or updating the build metadata, follow the :ref:`blueprints
workspace documentation <Workspace>`. Doing so, brings everything you need on your host to start building an image for this blueprint.

.. note::

   Ensure to `CHECKOUT_DIR` variable in the workspace documentation as you need the variable during the process

Build the Oniro Image for the Gateway
-------------------------------------

Once you have a workspace initialized as per the instructions in the preceding section, you are ready to build the OS. Firstly, you initialize a build:

.. code-block:: bash

   $ cd $CHECKOUT_DIR
   $ TEMPLATECONF=../oniro/flavours/linux . ./oe-core/oe-init-build-env build-oniro-gateway

Executing the command sets up a new build environment in the `build-oniro-gateway` directory (or reuse it, if it already exists).

Add the `meta-oniro-blueprints` layer to the build. This is only needed once
after initializing a new build:

.. code-block:: bash

   $ bitbake-layers add-layer $CHECKOUT_DIR/meta-homeassistant
   $ bitbake-layers add-layer $CHECKOUT_DIR/meta-oniro-blueprints

Once the build environment is set, start building the image.

.. code-block:: bash

   $ DISTRO=oniro-linux-blueprint-gateway MACHINE=raspberrypi4-64 bitbake blueprint-gateway-image

Once the build finishes, the image is ready to be written on a microSD card.

Flashing the Gateway Image
--------------------------

Ensure that you have plugged in an microSD card via an microSD card
reader attached to your host. Once that is done, note its associated device
node. You can find that using `udev`, `dmesg` or various other tools. Once the
device node associated to the microSD card is known, proceed to flash the built
image.

.. warning::

      The following commands assume that the device node associated with the
      microSD card is provided via the `DEVICE` environment variable. Make sure
      you have the correct one set before running the commands to avoid
      risking the loss of data.

      The image to flash is a full disk image, so the `DEVICE` variable needs to
      point to the entire block device node, and not to a specific partition.

.. code-block:: bash

   $ sudo bmaptool copy ./tmp/deploy/images/raspberrypi4-64/blueprint-gateway-image-raspberrypi4-64.wic.bz2 "$DEVICE"

Build the Oniro Image for the Mesh Node
---------------------------------------

Once you have a workspace initialised as per the instructions above, you are
ready to build the OS. The following command sets up a new build environment in the `build-oniro-mesh-node` directory (or reuse it, if it already exists):

.. code-block:: bash

   $ cd $CHECKOUT_DIR
   $ TEMPLATECONF=../oniro/flavours/zephyr . ./oe-core/oe-init-build-env build-oniro-mesh-node

Add the `meta-oniro-blueprints` layer to the build. This is only needed once
after initializing a new build:

.. code-block:: bash

   $ bitbake-layers add-layer $CHECKOUT_DIR/meta-homeassistant
   $ bitbake-layers add-layer $CHECKOUT_DIR/meta-oniro-blueprints

Once the build environment is set, start building the image.

.. code-block:: bash

   $ DISTRO=oniro-zephyr-blueprint-gateway MACHINE=arduino-nano-33-ble bitbake zephyr-blueprint-gateway-node

Once the build finishes, the image is ready to be flashed.

Flashing the Mesh Node Image
----------------------------

To flash the Arduino Nano 33 BLE device it first needs to enter the bootloader
to allow flashing. Connect the USB cable and double press the button. An orange
LED fading in and out indicates the device is ready to be flashed.

You might need to install the `python3-serial` package installed on your host
OS for the flashing task below..
To access the USB device for flashing your user needs to be part of the dialout
group. You need to logout and back in again for this change to take effect. If
you want to avoid this, temporarily change the ownership for the device
to allow flashing from a normal user account.

.. code-block:: bash

   $ sudo usermod -a -G dialout `whoami`
   $ sudo chown `whoami` /dev/ttyACM0
   $ DISTRO=oniro-zephyr-blueprint-gateway MACHINE=arduino-nano-33-ble bitbake zephyr-blueprint-gateway-node -c flash_usb

Prepare Makerdiary nRF52840 MDK USB dongle
------------------------------------------

As the last preparation step, you need to build and flash the firmware for the Makerdiary nRF52840 MDK USB dongle to allow it to act as an OpenThread radio for the border router. More details on this can be found in the following :ref:`section <MDK-rcp-flash>`.

Run the Gateway Blueprint
*************************

At this point, we have all the pieces we need to run the entire blueprint.
Plug in the microSD card on which you flashed the OS into the board's microSD
card slot. That brings us to the last step: booting the board by attaching the
power source. On a Raspberry Pi 4B, that would be a USB-C power supply.

The Arduino Nano 33 BLE board does not need a dedicated power supply, it
runs off the USB connection that you have for serial access.

With both devices up and running, now switch to demonstrate the mesh node onboarding process onto the OpenThread network. A full video tutorial on the Gateway blueprint, with details on the onboarding process, can be found :ref:`here <ResourcesTG>`.

Install Thread Commissioner App on Android Phone
------------------------------------------------

The Thread specification allows an external commissioner, in our case an Android based phone, to onboard headless devices which have no means to enter the needed credentials by themselves.

For this purpose, the Thread group released an Android app, which is used in the blueprint too, and can be found in Android Playstore at: https://play.google.com/store/apps/details?id=org.threadgroup.commissioner.

Generate QR Code for Commissioner Onboarding
--------------------------------------------

The credential and unique hardware identifiers used for the onboarding process are passed to the commissioner app in form of an QR code (in a final product, this is expected to be printed on the packaging or quick setup guide). Details on how to gather the information and generate the QR code is documented here :ref:`section <QR-gen>`.

Onboarding Process
------------------

The onboarding process is divided in the following steps:

#. Open the Android settings and connect the mobile phone to the Oniro Project WiFi. The Wi-Fi password is: **12345678**

.. image:: assets/gateway-commissioner-1.jpg
   :alt: Transparent Gateway - Connecting to WiFi

#. Start the Thread application on your phone. It shows two available OpenThread border routers. Select the one with the IP address 172.16.47.1. When accessing the border router, enter the Thread admin password as: J01NME.

.. image:: assets/gateway-commissioner-2.jpg
   :alt: Transparent Gateway - Thread Commissioner App

#. The camera view gets enabled and allows to scan the generated QR code for the mesh node.
#. Once the screen changes to **Adding My Thread Product to OniroThread**, press the button of the Arduino Nano 33 BLE to reset the device and finish the onboarding process.
#. The Thread app screen changes to **Added My Thread Product to OniroThread**.

.. image:: assets/gateway-commissioner-3.jpg
   :alt: Transparent Gateway - Thread device onboarded

Congratulations! you have successfully implemented the Transparent gateway blueprint. Now is a good time to explore the system further, and start extending it for your own needs.

Gateway Configuration Details
*****************************

.. toctree::
   :maxdepth: 3

   transparent-gateway-details.rst

Resources
*********

.. _ResourcesTG:

- https://www.youtube.com/watch?v=o_3ITbSAvNg
